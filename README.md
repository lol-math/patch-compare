# Patch Diff
Compare items, champions, and some other things, of different patches. Uses ddragon.

# Demo
[Click me](https://patchdiff.lolmath.net/)
