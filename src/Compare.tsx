import React, { useState } from "react";
import { DiffEditor } from "@monaco-editor/react";
import Dd from "ddragon";
import { useMemo } from "react";
import { useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import "./Compare.css";

interface VersionSelectProps {
  values: string[];
  onChange: (v: string) => void;
  value: string;
}

function Select({ values, onChange, value }: VersionSelectProps) {
  return (
    <select value={value} onChange={(e) => onChange(e.target.value)}>
      {values.map((_value) => (
        <option value={_value} key={_value}>
          {_value}
        </option>
      ))}
    </select>
  );
}

type DataKey = Exclude<keyof InstanceType<typeof Dd>["data"], "champion">;

const useDdragonData = (version: string, dataKey: DataKey) => {
  const [data, setData] = useState("");

  useEffect(() => {
    if (version === "?") {
      return;
    }
    const dd = new Dd(version);

    fetch(dd.data[dataKey]())
      .then((response) => response.json())
      .then((json) => {
        setData(JSON.stringify(json, null, 2));
      });
  }, [version, dataKey]);

  return data;
};

export function Compare() {
  const [searchParams, setSearchParams] = useSearchParams({
    dataKey: "champions",
  });
  const [versions, setVersions] = useState<string[]>([]);

  const dataKey = searchParams.get("dataKey") as DataKey;
  const originalVersion = searchParams.get("originalVersion") as string;
  const modifiedVersion = searchParams.get("modifiedVersion") as string;

  const updateSearchParams = (obj: Record<string, string>) => {
    setSearchParams({
      dataKey,
      originalVersion,
      modifiedVersion,
      ...obj,
    });
  };

  const dataKeys = useMemo(() => {
    const dd = new Dd(versions[0]);
    return Object.keys(dd.data).filter((v) => v !== "champion");
  }, []);

  const original = useDdragonData(originalVersion, dataKey);
  const modified = useDdragonData(modifiedVersion, dataKey);

  useEffect(() => {
    const dd = new Dd();
    fetch(dd.versions())
      .then((res) => res.json())
      .then((versions) => {
        setVersions(versions);
        if (!originalVersion || !modifiedVersion) {
          updateSearchParams({
            originalVersion: originalVersion || versions[1],
            modifiedVersion: modifiedVersion || versions[0],
          });
        }
      });
  }, []);

  return (
    <div className="compare">
      <div className="menu-bar">
        <span style={{ marginRight: 20 }}>LoL Ddragon Patch Diff</span>
        <Select
          onChange={(v) => updateSearchParams({ dataKey: v })}
          value={dataKey}
          values={dataKeys}
        />
        <Select
          onChange={(v) => updateSearchParams({ originalVersion: v })}
          value={originalVersion}
          values={versions}
        />
        <Select
          onChange={(v) => updateSearchParams({ modifiedVersion: v })}
          value={modifiedVersion}
          values={versions}
        />
        <a href="https://lolmath.net" style={{ marginLeft: 20 }}>
          lolmath
        </a>
      </div>
      <DiffEditor original={original} modified={modified} language="json" />
    </div>
  );
}
