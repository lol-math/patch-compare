import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Compare } from "./Compare";

function App() {
  return (
    <BrowserRouter>
      <Compare />
    </BrowserRouter>
  );
}

export default App;
